from time import sleep
from django.test import TestCase

from datetime import datetime, timedelta
import json
from django.test import TestCase
from api.models import Client, Mailing
from django.contrib.auth import get_user_model
import random
import pytz


class APITest(TestCase):
    base_url_clients = '/api/clients/'
    base_url_mailings = '/api/mailings/'

    def test_creates_client(self):
        tags = ["old", "new", "cool"]
        for i in range(10):
            number = random.randint(7_000_000_00_00, 7_999_999_99_99)
            carrier_tag = str(number)[1:4]
            self.client.post(self.base_url_clients, {'phone_number': number,
                                                     'carrier_tag': carrier_tag,
                                                     'tag': f'{random.choice(tags)}',
                                                     'timezone': f'{random.choice(pytz.all_timezones)}'})

        print(self.client.get('/api/clients/').content)
        self.assertEqual(Client.objects.count(), 10)
        response = self.client.post(self.base_url_mailings, {"start_time": datetime.now()-timedelta(30),
                                                 "stop_time": datetime.now()-timedelta(15),
                                                 "text": "previous",
                                                 "client_filter": json.dumps({"tag": "new"})})