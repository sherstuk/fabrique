from django.contrib.auth.models import User, Group
from rest_framework import serializers
from api.models import Client, Mailing, Message

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class MailingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mailing
        fields = ['id', 'start_time', 'stop_time', 'text', 'client_filter']

        def create(self, validated_data):
            return Mailing.objects.create(**validated_data)

class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'carrier_tag', 'tag', 'timezone']

        def create(self, validated_data):
            return Client.objects.create(**validated_data)


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'sent_date', 'status', 'client', 'mailing_list']

        def create(self, validated_data):
            return Message.objects.create(**validated_data)