
from rest_framework import viewsets
from rest_framework import permissions
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from django.shortcuts import redirect
from api.models import Client, Mailing, Message
from api.serializers import ClientSerializer, MailingSerializer, MessageSerializer
from django.contrib.auth.models import User
from django.contrib.auth import login
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets, mixins
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema

@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_summary="Получение списка всех рассылок"
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_summary="Создание рассылки",
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    operation_summary="Обновление атрибутов рассылки",
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    operation_summary="Обновление атрибутов рассылки",
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    operation_summary="Удаление рассылки",
))
class MailingViewSet(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = [permissions.AllowAny]

    @swagger_auto_schema(operation_summary="Получение статистики сообщениям конкретной рассылки")
    @action(detail=True, methods=['get'])
    def mailing_stats(self, request, pk=None):
        mailing = self.get_object()
        total_messages = Message.objects.filter(mailing_list=mailing)
        sent_messages = total_messages.filter(status="SENT")
        not_sent_messages = total_messages.filter(status="NOT SENT")
        details = []
        for message in total_messages:
            details.append(
                {"message_id": message.id,
                 "phone_number": message.client.phone_number,
                 "message_status": message.status})
        data = {"total_messages_count": total_messages.count(),
                "sent_messages_count": sent_messages.count(),
                "not_sent_messages": not_sent_messages.count(),
                "details": details}
        return Response(data=data)

    @swagger_auto_schema(operation_summary="Получение статистики по всем сообщениям")
    @action(detail=False, methods=['get'])
    def stats(self, request):
        total_messages = Message.objects.all()
        sent_messages = total_messages.filter(status="SENT")
        not_sent_messages = total_messages.filter(status="NOT SENT")
        data = {"total_messages_count": total_messages.count(),
                "sent_messages_count": sent_messages.count(),
                "not_sent_messages": not_sent_messages.count()}
        return Response(data=data)

@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_summary="Получение списка всех клиентов"
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_summary="Добавление клиента",
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    operation_summary="Изменение атрибутов клиента",
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    operation_summary="Изменение атрибутов клиента",
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    operation_summary="Удаление клиента",
))
class ClientViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    viewsets.GenericViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.AllowAny]
    filter_backends = [DjangoFilterBackend]

def quick_auth(request):
    return redirect('/api')
