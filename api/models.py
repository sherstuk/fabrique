from django.db import models

class Mailing(models.Model):
    """Модель рассылки
    """

    start_time = models.DateTimeField()
    stop_time = models.DateTimeField()
    text = models.CharField(max_length=64)
    client_filter = models.JSONField(max_length=128)

class Client(models.Model):
    """Модель Клиента
    """

    phone_number = models.PositiveIntegerField()
    carrier_tag = models.PositiveIntegerField()
    tag = models.CharField(max_length=64)
    timezone = models.CharField(max_length=32, default='UTC')

class Message(models.Model):
    """Модель Уведомления
    """
    
    sent_date = models.DateTimeField(null=True)
    status = models.CharField(max_length=10)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    mailing_list = models.ForeignKey(Mailing, on_delete=models.CASCADE)
