from celery import shared_task
from celery.utils.log import get_task_logger
from django.utils import timezone
from requests.exceptions import ConnectionError
import requests
import json
import os
from api.models import Client, Message, Mailing

logger = get_task_logger(__name__)

@shared_task
def detect_actual_mailings():
    """Задание на обнаружение текущих рассылок и отправки уведомлений для новых
    """

    current_mailings = Mailing.objects.filter(start_time__lte=timezone.now()).filter(stop_time__gte=timezone.now())
    logger.info(f'Detected {current_mailings.count()} actual mailings')

    # Из актуальных рассылок ищем те, которые не запускались
    for mailing in current_mailings:
        messages_of_mailing = Message.objects.filter(mailing_list=mailing).count()
        if messages_of_mailing == 0:
            # Даём задание на рассылку сообщений всем клиентам этой рассылки
            send_messages_for_new_mailing(mailing)
        else:
            # Даём задание на переотправку неотправленных сообщений для этой рассылки
            resend_messages_for_mailing(mailing)

@shared_task
def resend_messages_for_mailing(mailing):
    """Повторная отправка недоставленных уведомлений в рассылке

    Args:
        mailing (Mailing(Model)): Модель Рассылки
    """

    unsend_messages = Message.objects.filter(mailing_list=mailing).filter(status="NOT SENT")
    for message in unsend_messages:
        task_send_message(message=message)


@shared_task
def send_messages_for_new_mailing(mailing):
    """Отправка уведомлений для новой рассылки

    Args:
        mailing (Mailing(Model)): Модель Рассылки
    """

    client_filter = dict(mailing.client_filter)
    carrier_tag = client_filter.get('carrier_tag', None)
    tag = client_filter.get('tag', None)
    clients = Client.objects.all()

    if carrier_tag:
        clients = clients.filter(carrier_tag=carrier_tag)
    if tag:
        clients = clients.filter(tag=tag)

    for client in clients:
        task_send_message(client=client, mailing=mailing)

@shared_task(bind=True, autoretry_for=(Exception,), retry_kwargs={'max_retries': 2, 'countdown': 5})
def task_send_message(self, client=None, mailing=None, message=None):
    """Задание на отправку одного уведомления одному клиенту 

    Args:
        client (Client(Model)): Модель Клиента
        mailing (Mailing(Model)): Модель Рассылки
        message (Message(Model)): Модель сообщения
    """

    my_token = {"Authorization": f"Bearer {os.environ.get('BEARER_TOKEN')}"}
    if message:
        pass
    elif client and mailing:
        message = Message.objects.create(client=client, mailing_list=mailing, status="NOT SENT")

    message_JSON = {"id": message.id, "phone": message.client.phone_number, "text": message.mailing_list.text}
    try:
        response = requests.post(f'https://probe.fbrq.cloud/v1/send/{message.id}', headers=my_token, data=json.dumps(message_JSON))
        if response.status_code == 200:
            message.sent_date = timezone.now()
            message.status="SENT"
            message.save()
            logger.info(f"Message {message.id} sent to {message.client.phone_number} in mailing {message.mailing_list.id}")
    except ConnectionError:
        logger.warning(f"Message {message.id} wasn't sent to {message.client.phone_number} in mailing {message.mailing_list.id}")

