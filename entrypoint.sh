#!/bin/sh

python manage.py flush --no-input
python manage.py makemigrations api
python manage.py migrate
python manage.py loaddata mailing.json
python manage.py loaddata clients.json
# python manage.py test

exec "$@"