from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from api import views

schema_view = get_schema_view(
    openapi.Info(
        title="API уведомлений",
        default_version='v1',
        description="",
        contact=openapi.Contact(email="sher-art@yandex.ru"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

router = routers.DefaultRouter()
router.register('mailing', views.MailingViewSet)
router.register('clients', views.ClientViewSet)
# router.register('messages', views.MessageViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls), name='api_root'),
    path('', views.quick_auth),
    path('docs/', schema_view.with_ui('swagger',
         cache_timeout=0), name='schema-swagger-ui'),
]
